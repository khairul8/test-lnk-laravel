<?php


namespace App\Models;

use App\Models\Users;
use Jenssegers\Mongodb\Eloquent\Model;


class Histories extends Model
{
	protected $connection = 'mongodb';
	protected $collection = 'histories';
    protected $primaryKey = '_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sessionId', 'user', 'dateLogin', 'dateLogout', 'username'
    ];

    // public function users()
    // {
    //     return $this->belongsTo('Histories');
    // }
}