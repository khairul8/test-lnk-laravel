<?php


namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;


class Users extends Model
{
	protected $connection = 'mongodb';
	protected $collection = 'users';
    protected $primaryKey = '_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password', 'date'
    ];

    // public function histories()
    // {
    //     return $this->hasMany(Histories::class, 'username', 'username');
    // }
}