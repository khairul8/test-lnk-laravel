<?php
   
namespace App\Http\Controllers;
 
use App\Models\Users;
use App\Models\Histories;
use Illuminate\Http\Request;
use Redirect,Response;
use Carbon\Carbon;
 
class TestLNKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $getUsers = Users::all();
        $result = array();
        foreach($getUsers as $datas) {
            $getHistories = Histories::where('username', $datas->username)->get();
            $histories = array();
            $totalDurationInSeconds = 0;

            if (count($getHistories) > 0) {
                foreach($getHistories as $rowHistory) {
                    $dateLogin = Carbon::createFromTimestampMs($rowHistory->dateLogin)->toDateTimeString();
                    $dateLogout = $rowHistory->dateLogout ? Carbon::createFromTimestampMs($rowHistory->dateLogout)->toDateTimeString() : Carbon::now()->toDateTimeString();
                    $diff = Carbon::parse($dateLogout)->diffInSeconds(Carbon::parse($dateLogin));
                    $rowHistory->durationInSeconds = $diff;
                    $totalDurationInSeconds+=$diff;
                }
                $histories = $getHistories;
            } else {
                $histories = [];
            }

            $datas->totalDurationInSeconds = $totalDurationInSeconds;
            $datas->histories = $histories;
            array_push($result, array(
                'name' => $datas->name,
                'username' => $datas->username,
                'totalDurationInSeconds' => $totalDurationInSeconds,
            ));
            
        }
         foreach($result as $row) {
            $data['label'][] = $row['username'];
            $data['data'][] = (int) $row['totalDurationInSeconds'];
        }
        $data['chart_data'] = json_encode($data);
        return view('test-lnk', $data);
    }
 
}