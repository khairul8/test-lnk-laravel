<!DOCTYPE html>
<html>
<head>
  <title>Test PT Lautan Natural Krimerindo</title>
  <!-- Latest CSS -->
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
 <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
 <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> 
</head>
<body>

    <div style="width: 1000px">
        <canvas id="bar-chart"></canvas>
    </div>
 
    <!-- javascript -->
    <script>
        $(function(){
            const cData = JSON.parse(`<?php echo $chart_data; ?>`);
            const ctx = $("#bar-chart");
        
            const data = {
                labels: cData.label,
                datasets: [
                    {
                        label: "Duration in Seconds",
                        data: cData.data,
                        borderWidth: [1, 1, 1, 1, 1,1,1]
                    }
                ]
            };
        
            const options = {
                responsive: true,
                legend: {
                    display: true,
                    position: "bottom",
                    labels: {
                        fontColor: "#333",
                        fontSize: 16
                    }
                }
            };
        
            const chart1 = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options
            });
        
        });
    </script>
</body>
</html>